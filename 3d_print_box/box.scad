echo(version=version());


module caja(alto,ancho,profundidad){
color("green")    
    translate([0, 0, 0])
    cara(2,23,23,0,0,0);
    difference(){
        linear_extrude(height = alto)
            square([ancho+2, profundidad+2], center = true);
        linear_extrude(height = alto)
            square([ancho, profundidad], center = true);
        //cara(2,2,23,0,0,0);
        cara(2,23,23,1,0,0);
        }
 }

module cara(alto,ancho,profundidad,x,y,z){
color("red")
    translate([x, y, z])
        rotate([0,0,0])
            linear_extrude(height = alto)
                square([ancho, profundidad], center = true);
        
 }
 
 
 cara(2,23,23,0,0,51);
 caja(50,23,23);
 //cara(2,23,23,0,0,0);
 //cara(2,2,23,-11,0,0);
 